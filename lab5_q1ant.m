% QUEST�O 1
% AUTORES: William C. Guarienti (230322) e Raul C. Athayde (246762)
%
% Considere um sistema representado pela seguinte equa��o diferencial:
%
%   y'''(t)+41y''(t)+360y'(t)+900y(t)=600x'(t)+1200x(t)
%
%  onde y(0)=2, y'(0)=1 e y''(0)=-0.05. Baseado neste sistema, pede-se:
%
%(a) Calcular a solu��o desta equa��o diferencial, ou seja, calcular a
%resposta natural, a resposta for�ada para um degrau unit�rio e a resposta
%completa, como visto na �rea 1 da disciplina.
%(b) Determinar a transformada de Laplace do sistema (realizar os c�lculos
%� m�o ou usando a fun��o 'laplace').
%(c) Atrav�s da transformada de Laplace inversa (fun��o 'residue' para
%fra��es parciais), obter as respostas natural, for�ada e completa do
%sistema. Simular e comparar com as respostas obtidas no item (a).
%(d) Usando a transformada de Laplace, simular as respostas natural,
%for�ada e completa usando os comandos 'step', 'impulse', ou 'lsim'. 
%Comparar com as repostas obtidas anteriormente.
%(e) Implementar um diagrama de blocos que representa a equa��o diferencial
%dada no simulink (usando blocos somadores, integradores e ganhos) e simule
%as repostas natural, for�ada e completa. Obtenha as mesmas repostas usando
%blocos de fun��es de transfer�ncia.
%
% CONCLUS�ES
% O item A apresenta um desafio interessante. A t�cnica usual de resolu��o
% de equa��es diferenciais n�o prev� o caso da entrada igual a um impulso.
% Dessa forma, utilizamos a ideia de que a derivada da resposta ao degrau �
% a resposta ao impulso e utilizamos do princ�pio da sobreposi��o.
% Para os item B e C, n�o houve nenhum problema que caiba ser destacado. Os
% resultados foram obtidos computacionalmente e verificados 'manualmente'.
% Atrav�s das fun��es step e impulso, obtivemos o resultado correto de
% forma bastante r�pida no item D, fazendo uso dos resultados obtidos em B.
% A vantagem em utilizar essas fun��es j� presentes no matlab � obter um
% resultado num�rico bastante preciso com pouco esfor�o de programa��o,
% ainda que com este m�todo n�o obtenhamos uma 'f�rmula fechada' para a
% solu��o. Na (E) verificamos a solu��o do problema utilizando diagramas de
% blocos (somadores, integradores e ganhos) e blocos de fun��o de
% transfer�ncia; para compar�-los, plotamos os resultados sobrepostos.


function lab5_q1

clc;
xx=linspace(0,10,1000);

%Quest�o A
disp('a)')
disp('yn(t)=[15,838*exp(-5t) - 13,95625*exp(-6t) + 0,11825*exp(-30t)]*u(t)')
figure(1)
yyn=yn(xx);
plot(xx,yyn);
title('Resposta Natural obtida manualmente')
xlabel('t')
ylabel('yn(t)')
grid on

disp('yf(t)=[14,385*exp(-5t) - 16,6667*exp(-6t) + 0,9333*exp(-30t) + 1,3333]*u(t)')
figure(2)
yyf=yf(xx);
plot(xx,yyf);
title('Resposta For�ada obtida manualmente')
xlabel('t')
ylabel('yf(t)')
grid on

disp('yc(t)=[30,223*exp(-5t) - 30.62291*exp(-6t) + 1.05158*exp(-30t) + 1.3333]*u(t)')
figure(3)
yyc=yyn+yyf;
plot(xx,yyc);
title('Resposta Completa obtida manualmente')
xlabel('t')
ylabel('yc(t)')
grid on

%Quest�o B

disp('b)')
syms s t Y X

LHS = laplace(diff(diff(diff(sym('y(t)'))))+41*diff(diff(sym('y(t)')))+360*diff(sym('y(t)')) + 900*sym('y(t)')) ;

newLHS = subs(LHS, {'laplace(y(t), t, s)','D(D(y))(0)', 'D(y)(0)','y(0)'}, {Y,-0.05,1,2});

RHS = laplace(600*diff(sym('x(t)')) + 1200*(sym('x(t)')));

newRHS = subs(RHS,{'laplace(x(t), t, s)','x(0)'},{1/s,0});

Y = solve(newLHS-newRHS,Y);

disp('Y(s)=');pretty(Y)

%Quest�o C

disp('c)')

%resposta natural
syms s t Y X

LHS = laplace(diff(diff(diff(sym('y(t)'))))+41*diff(diff(sym('y(t)')))+360*diff(sym('y(t)')) + 900*sym('y(t)')) ;

newLHS = subs(LHS, {'laplace(y(t), t, s)','D(D(y))(0)', 'D(y)(0)','y(0)'}, {Y,-0.05,1,2});

Y = solve(newLHS,Y);

Y=ilaplace(Y);

disp('resposta natural')

pretty(Y)

%resposta for�ada ao degrau
syms s t Y X

LHS = laplace(diff(diff(diff(sym('y(t)'))))+41*diff(diff(sym('y(t)')))+360*diff(sym('y(t)')) + 900*sym('y(t)')) ;

newLHS = subs(LHS, {'laplace(y(t), t, s)','D(D(y))(0)', 'D(y)(0)','y(0)'}, {Y,0,0,0});

RHS = laplace(600*diff(sym('x(t)')) + 1200*(sym('x(t)')));

newRHS = subs(RHS,{'laplace(x(t), t, s)','x(0)'},{1/s,0});

Y = solve(newLHS-newRHS,Y);

Y=ilaplace(Y);

disp('resposta for�ada')

pretty(Y)

%resposta completa
syms s t Y X

LHS = laplace(diff(diff(diff(sym('y(t)'))))+41*diff(diff(sym('y(t)')))+360*diff(sym('y(t)')) + 900*sym('y(t)')) ;

newLHS = subs(LHS, {'laplace(y(t), t, s)','D(D(y))(0)', 'D(y)(0)','y(0)'}, {Y,-0.05,1,2});

RHS = laplace(600*diff(sym('x(t)')) + 1200*(sym('x(t)')));

newRHS = subs(RHS,{'laplace(x(t), t, s)','x(0)'},{1/s,0});

Y = solve(newLHS-newRHS,Y);

Y=ilaplace(Y);

disp('resposta completa')

pretty(Y)

%quest�o D

t=linspace(0,10,1000);

num=[2 83 15219/20];
den=[1 41 360 900 ];
H=tf(num,den);
rn=impulse(H,t);

figure(4)
plot(t,rn);
title('Resposta Natural obtida computacionalmente')
xlabel('t')
ylabel('yn(t)')
grid on;

num=[600 1200];
den=[1 41 360 900];

H=tf(num,den);
rf=step(H,t);

figure(5)
plot(t,rf);
title('Resposta For�ada obtida computacionalmente')
xlabel('t')
ylabel('yf(t)')
grid on;

figure(6)
rc=rn+rf;
plot(t,rc);
title('Resposta Completa obtida computacionalmente')
xlabel('t')
ylabel('yc(t)')
grid on;
end

function y=yn(t)
y =15.838*exp(-5*t) - 13.95625*exp(-6*t) + 0.11825*exp(-30*t);
end

function y=yf(t)
y = 14.385*exp(-5*t) - 16.6667*exp(-6*t) + 0.93333*exp(-30*t) + 1.3333;
end


