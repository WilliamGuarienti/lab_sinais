%QUEST�O 2
%   AUTORES: William C. Guarienti (230322) e Raul C. Athayde (246762)
%
%ENUNCIADO:
%Considere um sistema representado pela seguinte equa��o de
%diferen�as (considerar o per�odo de amostragem Ts=0.1):
%           y[n] - 1.63y[n-1] + 0.663y[n-2] = x[n] - 0.8x[n-2]
%onde y[-1]=4 e demais condi��es iniciais s�o nulas. Baseado neste sistema,
%pede-se:
%
%(a) Simulas as respostas natural, for�ada para um degrau unit�rio e
%completa atrav�s da simula��o da equa��o de recorr�ncia.
%(b) Calcular a solu��o da equa��o de diferen�as, ou seja, calcular a
%resposta natural, a resposta for�ada para um degrau unit�rio e a resposta
%completa, como visto na �rea 1 da disciplina. Comparar as respostas
%obtidas com as do item anterior.
%(c) Determinar a transformada Z do sistema (realizar os c�lculos � m�o ou
%atrav�s da fun��o ztrans).
%(d) Obter a transformada inversa do sistema para uma entrada x[n]=u[n] e
%condi��es iniciais nulas atrav�s do m�todo das divis�es sucessivas (tamb�m
%chamado de S�rie de Pot�ncias) e comparar com o resultado obtido pela
%fun��o step. At� qual coeficiente deve-se dividir o sistema para termos
%uma boa aproxima��o da resposta?
%(e) Obter a transformada inversa do sistema para entrada nula e condi��es
%iniciais diferentes de zero atrav�s do m�todo das divis�es sucessivas e
%comparar com o resultado obtido pela fun��o impulse. At� qual
%coeficiente deve-se dividir o sistema para termos uma boa aproxima��o da
%resposta?
%(f) Obter a transformada inversa do sistema para uma entrada x[n]=u[n]
%atrav�s do m�todo das fra��es parciais (comando residuez para fra��es
%parciais). Calcule a resposta natural e a resposta for�ada do sistema.
%Compare com o resultado do item anterior.
%(g) Implementar um diagrama de blocos que representa a equa��o de
%diferen�as dada no simulink (usando blocos somadores, de deslocamento e
%ganhos) e simule as respostas natural, for�ada e completa. Obtenha as
%mesmas respostas usando blocos de fun��es de transfer�ncia.
%
%CONCLUS�O:
%    No item A, resolvemos o problema numericamente. Esse m�todo � bastante
%    interessante para calcularmos os primeiros termos da sequ�ncia, mas �
%    extremamente ineficaz se requerermos apenas dados sobre pontos
%    distantes do zero. No item B, resolvemos o problema de encontrar as
%    ra�zes de encontrar as ra�zes da equa��o caracter�stica e os
%    coeficientes que multiplicam tais ra�zes. Resolvemos tal problema
%    utilizando apenas o computador e obtemos resultados muito bons (ainda
%    que as ra�zes n�o tenham sido calculadas com exatid�o, pois foi usado
%    um m�todo num�rico que deixou um erro - ainda que insignificante - na
%    resposta). No item C, obtivemos a transformada Z da sa�da tamb�m
%    computacionalmente, obtendo resultados concordantes com os que
%    obtivemos 'no papel'. No item D e E, utilizamos o m�todo das s�ries de
%    pot�ncia, obtendo resultados bastante precisos; tamb�m utilizamos as
%    fun��es step e impulse do matlab. No item F, procedemos a expans�o em
%    fra��es parciais da transformada z da resposta natural e da resposta
%    for�ada. Novamente, verificamos bons resultados. Por fim, no item G,
%    resolvemos o problema usando blocos e usando as fun��es de
%    transfer�ncia. Notamos que n�o havia um bloco de sinal 'impulso';
%    dessa forma, constru�mos um impulso como a subtra��o de dois degraus,
%    um atrasado do outro em uma unidade. 
%    

clear all;
clc;

%A)
%Resposta natural:
yn(1)=0;            %1: refere-se ao �ndice -2; 2: refere-se ao �ndice -1, 3: refere-se ao �ndice 0...
yn(2)=4;            
u= @(n) (n>=3);     %Define o step     
for i=3:100
    yn(i)=1.63*yn(i-1)-0.663*yn(i-2);
end

figure(1)
subplot(3,1,1);
stairs(0:1:50,yn(3:53))
title('Resposta natural');
ylabel('y_n [n]');
xlabel('n');

%Resposta for�ada:
yf(1)=0;
yf(2)=0;
for i=3:100
    yf(i)=1.63*yf(i-1)-0.663*yf(i-2)+u(i)-0.8*u(i-2);
end    

subplot(3,1,2);
stairs(0:1:50,yf(3:53))
title('Resposta for�ada');
ylabel('y_f [n]');
xlabel('n');


%Resposta completa:
yc(1)=0;
yc(2)=4;
for i=3:100
    yc(i)=1.63*yc(i-1)-0.663*yc(i-2)+u(i)-0.8*u(i-2);
end    

subplot(3,1,3);
stairs(0:1:50,yc(3:53))
title('Resposta completa');
ylabel('y_c [n]');
xlabel('n');

%B)
%Resposta natural:
p=[1 -1.63 0.663];
r= roots(p);        %ra�zes da eq. caracter�stica
An = [(r(1))^-2 , (r(2))^-2 ; (r(1))^-1 , (r(2))^-1];
Bn = [0;4];
xn = An\Bn;         %calculo das constantes...
ynn = @(n)   (xn(1)*r(1).^n + xn(2)*r(2).^n).*(n>=0);

%Resposta for�ada:
% Af = [1 ,1 ,1 ,0 ; 0.78, 0.85, 1, 0; 0.78^2 , 0.85^2, 1,1 ; 0.78^3 , 0.85^3 , 1 , 1];
% Bf = [yf(3);  1+1.63*yf(3); 0.2+1.63*yf(4)-0.663*yf(3) ; 0.2+1.63*yf(5)-0.663*yf(4)];
% xf = Af\Bf; 
Af = [1 ,1 ,1; 0.78, 0.85, 1; 0.78^2 , 0.85^2, 1];
Bf = [yf(3);  1+1.63*yf(3); 0.2+1.63*yf(4)-0.663*yf(3)];
xf = Af\Bf; 
yff = @(n)  (xf(1)*0.78.^n + xf(2)*0.85.^n + xf(3)).*(n>=0); 

%Resposta completa: soma das respostas for�ada e homogenea
ycc = @(n) + ((xn(2)+xf(1))*0.78.^n + (xn(1)+xf(2))*0.85.^n + xf(3)).*(n>=0); 

figure(2)
subplot(3,1,1);
stairs(0:1:50,ynn(0:50))
title('Resposta natural');
ylabel('y_n [n]');
xlabel('n');

subplot(3,1,2);
stairs(0:1:50,yff(0:50))
title('Resposta for�ada');
ylabel('y_f [n]');
xlabel('n');

subplot(3,1,3);
stairs(0:1:50,ycc(0:50))
title('Resposta completa');
ylabel('y_c [n]');
xlabel('n');
    
%C�lculo dos erros
for i=3:53    
en(i-2) = ynn(i-3) - yn(i);       %aten��o! ynn � uma fun��o, yn � um vetor
ef(i-2) = yff(i-3) - yf(i);
ec(i-2) = ycc(i-3) - yc(i);
end

figure(3)
%Comparando as respostas B e A...
subplot(3,1,1)
stairs(0:1:50,en)
title('Compara��o da resposta natural');
ylabel('e_n [n]');
xlabel('n');

subplot(3,1,2)
stairs(0:1:50,ef)
title('Compara��o da resposta for�ada');
ylabel('e_f [n]');
xlabel('n');

subplot(3,1,3)
stairs(0:1:50,ec)
title('Compara��o da resposta completa');
ylabel('e_c [n]');
xlabel('n');


%ITEM C)
%Tirando a transformada Z da sa�da utilizando o computador... (resposta
%completa)
% O sistema � causal, logo a sa�da � lateral direita; logo, podemos tirar a transformada z unilateral
%Fonte: https://goo.gl/TnhdbA ;
%http://www.cpp.edu/~zaliyazici/ece308/matlab-2.pdf; http://ctms.engin.umich.edu/CTMS/index.php?aux=Extras_Diffeq
syms z Y n x X(z)
LHS = ztrans(sym('y(n)')-1.63*sym('y(n-1)')+0.663*sym('y(n-2)'),n,z);
RHS = ztrans(sym('x(n)')-0.8*sym('x(n-2)'));
newLHS = subs(LHS,{'ztrans(y(n),n,z)','y(-1)','y(-2)'},{Y,4,0});
newRHS = subs(RHS,{'ztrans(x(n),n,z)','x(-1)','x(-2)'},{X(z),0,0});
Y = solve(newLHS-newRHS,Y);
disp('Item C)');
disp('A transformada Y(z) � expressa por:')
pretty(Y)

%D) S�o necess�rios, aproximadamente, 50 coeficientes para fornecer um
%resultado interessante.
num = [1 0 -0.8];
den = [1 -1.63 0.663];
%Comparando a resposta com a resposta do item (A) com sample time
%indeterminado.
disp('Item D)');
disp('A fun��o de transfer�ncia H(z) do sistema � expressa por:')
H = tf(num,den,-1)

%step(sys,Tfinal) simulates the step response from t = 0 to the final time
%t = Tfinal. Express Tfinal in the system time units, specified in the
%TimeUnit property of sys. For discrete-time systems with unspecified
%sample time (Ts = -1), step interprets Tfinal as the number of sampling
%periods to simulate.

stp = step(H,50);
stp = stp'; %transp�em o vetor para torn�-lo um vetor-linha
%Redifinindo a fun��o de transfer�ncia com o sample time fornecido pelo enunciado.
%H = tf(num,den,0.1);

%Para uma entrada x[n]=u[n]
%substituindo X(z) por z/(z-1)
numu = [num 0];                  %Multiplica��o do numerador por z
denu = conv([1,-1],den);         %Multiplica��o do denominador por z-1
%Fazendo a divis�o polinomial computacionalmente...
numu = [numu zeros(1,70-1)];
[x,resto] = deconv(numu,denu);

figure(4)
subplot(2,1,1);
stairs(0:1:50,stp(1:51));
hold on
stairs(0:1:50,x(1:51),'g-.');
title('Resposta ao degrau unit�rio usando a fun��o step e o m�todo das divis�es sucessivas'); 
ylabel('y_f [n]');
xlabel('n');
legend('Gerado pela fun��o step','Gerado pelo m�todo das divis�es sucessivas');

subplot(2,1,2)
stairs(0:1:50,stp(1:51)-x(1:51));
title('Compara��o entre as respostas:')
ylabel('e_f [n]');
xlabel('n');

%E) Com um n�mero de coeficientes entre 50 e 60 obtemos um resultado
%aceit�vel para a resposta natural. Aqui, resolvemos exibir 60; no restante
%do c�digo, exibimos 50 coeficientes. Assim como na D), o n�mero de
%coeficientes necess�rios para uma boa aproxima��o depende do que chamamos
%de 'boa aproxima��o'.
Yn = subs(Y,{'X(z)'},{0});
numn = [6520 -2652 0];
demn = [1000 -1630 663];
tzn = tf(numn,demn,-1);
%Multiplicando tzn por 1 (Tz do impulso)
tzn = impulse(tzn,60);
tzn = tzn';
numn = [numn, zeros(1,70-1)];
[x,resto] = deconv(numn,demn);

figure(5)
subplot(2,1,1);
stairs(0:1:60,tzn(1:61));
hold on
stairs(0:1:60,x(1:61),'g-.');
title('Resposta natural usando a fun��o impulse e o m�todo das divis�es sucessivas'); 
ylabel('y_f [n]');
xlabel('n');
legend('Gerado pela fun��o impulse','Gerado pelo m�todo das divis�es sucessivas');

subplot(2,1,2)
stairs(0:1:60,tzn(1:61)-x(1:61));
title('Compara��o entre as respostas:')
ylabel('e_f [n]');
xlabel('n');


%F) Transformada inversa atrav�s do m�todo de fra��es parciais
%Redefinindo apenas para garantir a corre��o da solu��o e por quest�es de
%organiza��o...
num = [1 0 -0.8];
den = [1 -1.63 0.663];
%Para uma entrada x[n]=u[n]
numu = [num 0];                  %Multiplica��o do numerador por z
denu = conv([1,-1],den);         %Multiplica��o do denominador por z-1
[rf,pf,kf] = residuez(numu,denu); 

%Para entrada nula
numn = [6520 -2652 0];
demn = [1000 -1630 663];
[rn,pn,kn] = residuez(numn,demn);

fy = @(n)   (rf(1)*pf(1).^n + rf(2)*pf(2).^n + rf(3)*pf(3).^n).*(n>=0);
ny = @(n)   (rn(1)*pn(1).^n + rn(2)*pn(2).^n).*(n>=0);
cy = @(n)   ((rf(2)+rn(1))*pf(2).^n + (rf(3)+rn(2))*pf(3).^n).*(n>=0); 

%comparando com o resultado do item anterior...
figure(6)
subplot(2,1,1);
stairs(0:1:50,fy(0:50));
hold on
stairs(0:1:50,stp(1:51),'g-.');
title('Resposta ao degrau unit�rio usando a fun��o residuez e usando a fun��o step '); 
ylabel('y_f [n]');
xlabel('n');
legend('Gerado a partir da fun��o residuez','Gerado pela fun��o step');

subplot(2,1,2);
stairs(0:1:50,ny(0:50));
hold on
stairs(0:1:50,tzn(1:51),'g-.');
title('Resposta natural usando a fun��o residuez e usando a fun��o impulse'); 
ylabel('y_n [n]');
xlabel('n');
legend('Gerado a partir da fun��o residuez','Gerado pela fun��o impulse');

open_system('simulacao_q2');


%   ------- POLINOMIOS DO NUMERADOR E DO DENOMINADOR QUE CONSTITUEM A RESPOSTA COMPLETA ---------
% syms z Y n x X(z)
% LHS = ztrans(sym('y(n)')-1.63*sym('y(n-1)')+0.663*sym('y(n-2)'),n,z);
% RHS = ztrans(sym('x(n)')-0.8*sym('x(n-2)'));
% newLHS = subs(LHS,{'ztrans(y(n),n,z)','y(-1)','y(-2)'},{Y,4,0});
% newRHS = subs(RHS,{'ztrans(x(n),n,z)','x(-1)','x(-2)'},{z/(z-1),0,0});
% %Polin�mios que constituem a transformada z da resposta completa:
% Y = solve(newLHS-newRHS,Y)
% %Substituindo manualmente...
% num = -( [0,conv([2652 0],[1 -1])]+ [0 0 800*[1 0]] - conv(1000*[1 0 0],[1 0]) -conv([6520 0 0],[1 -1]) );
% den = conv([1000, -1630, 663],[1 -1])
